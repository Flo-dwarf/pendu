import "./App.css";
import React from "react";
import Mots from "./mots.json";

const word = Mots.mot[Math.floor(Math.random() * Mots.mot.length)]
  .normalize("NFD")
  .replace(/[\u0300-\u036f]/g, "") //enlève les accents
  .toUpperCase();

class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      letters: "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
      guessWord: word,
      hiddenLetter: "_".repeat(word.length),
      guessLetter: "",
      count: 0,
      eventKey: [],
    };
  }

  handleNewWord() {
    const newWord = Mots.mot[Math.floor(Math.random() * Mots.mot.length)]
      .normalize("NFD")
      .replace(/[\u0300-\u036f]/g, "") //enlève les accents
      .toUpperCase();

    this.setState({
      guessWord: newWord,
      hiddenLetter: "_".repeat(newWord.length),
      count: 0,
      eventKey: [],
    });
    const input = document.getElementById("inputFocus");
    if (input) {
      input.focus();
    }
  }

  handleChange(event) {
    const guessWord = [...this.state.guessWord];
    const eventKey = [...this.state.eventKey];
    const letterChoose =
      event.target.value.toUpperCase() || event.target.textContent; //value input || value button
    let letterPosition = guessWord.indexOf(letterChoose);

    if (letterPosition > -1) {
      this.setState((statePrevious) => {
        const hiddenLetter = [...statePrevious.hiddenLetter];
        for (let i = 0; i < guessWord.length; i++) {
          if (guessWord[i] === letterChoose) {
            hiddenLetter[i] = letterChoose;
          }
        }
        return { hiddenLetter, guessLetter: "" };
      });
      return;
    } else {
      eventKey.push(letterChoose);
      this.setState({
        count: this.state.count + 1,
        eventKey,
      });
    }
    this.setState({ guessLetter: "" });
  }

  handleFocus(event) {
    const button = document.querySelector("button");
    const buttonFocus = document.getElementById("buttonFocus");
    const input = document.getElementById("inputFocus");

    if (event.key === "Enter" && buttonFocus) {
      buttonFocus.focus();
      if (input) {
        input.focus();
      }
    } else {
      setTimeout(() => {
        button.blur();
      }, 500);
      if (input) {
        input.focus();
      }
    }
  }

  render() {
    const { count, guessLetter, hiddenLetter, letters, eventKey } = this.state;
    console.log(this.state.guessWord);

    window.addEventListener("keydown", (event) => {
      this.handleFocus(event);
    });

    // start - Find unique string duplicates in array Mot (again 350)
    // let findDuplicates = (array) =>
    //   array.filter((item, index) => array.indexOf(item) !== index);
    // console.log([...new Set(findDuplicates(Mots.mot))]);
    // end - Find unique string duplicates in array Mot

    return (
      <div className="App">
        <h2>Jeu du Pendu</h2>
        <div id="wordToGuess">
          {[...hiddenLetter].map((letter, index) => (
            <span key={index} className="hidden all">
              {letter}
            </span>
          ))}
        </div>
        <>
          {!hiddenLetter.includes("_") ? (
            <>
              <h1 className="marginWord boldWord">ᕦ(ツ)ᕤ</h1>
              <h3 className="marginWord boldWord">YOU WIN !</h3>
            </>
          ) : count < 11 ? (
            <React.Fragment>
              <div className="all">Nombre d'essai : {count} /11</div>
              <input
                autoFocus
                type="text"
                id="inputFocus"
                value={guessLetter}
                className="all"
                placeholder="Lettre..."
                onChange={this.handleChange.bind(this)}
              />

              <ul>
                {[...letters].map((letter) => (
                  <li key={letter}>
                    <button
                      id={letter}
                      onClick={this.handleChange.bind(this)}
                      disabled={hiddenLetter.includes(letter)}
                    >
                      {letter}
                    </button>
                  </li>
                ))}
              </ul>

              <>
                <div className="marginWord">Mauvais choix :</div>
                {eventKey.length > 1 ? (
                  <div className="marginWord boldWord">{eventKey}</div>
                ) : (
                  <div className="marginWord">_</div>
                )}
              </>
            </React.Fragment>
          ) : (
            <React.Fragment>
              <>
                <h1 className="marginWord">¯\_(ツ)_/¯</h1>
                <h3 className="marginWord">PERDU !</h3>
                <div className="marginWord">
                  Le mot recherché était
                  <span className="boldWord"> {this.state.guessWord}</span>
                </div>
              </>
            </React.Fragment>
          )}
        </>
        <>
          <button
            id="buttonFocus"
            onClick={() => this.handleNewWord()}
          >
            Générer un nouveau mot
          </button>
        </>
      </div>
    );
  }
}

export default App;
